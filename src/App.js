import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'


import Navbar from './components/Navbar';
import Home from './pages/Home'
import View from './pages/View'
import List from './pages/List'
import Add from './pages/Add'

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <div className="container my-5">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/add" component={Add} />
          <Route path="/list" component={List} />
          <Route path="/view/:id" component={View} />
        </Switch>
      </div>
    </BrowserRouter>
    
  );
}

export default App;
