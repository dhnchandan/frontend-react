import React from 'react';
import {Link} from 'react-router-dom'

const User = ({user}) => {
	return(
		<tr key={user.id}>
            <td>{user.name}</td>
            <td>{user.email}</td>
            <td>{user.phone}</td>
            <td>{user.address}</td>
            <td>{user.city}</td>
            <td>{user.street}</td>
            <td>{user.country}</td>
            <td>
                <Link to={`/view/${user.id}`} className="btn btn-info btn-sm">View</Link>
            </td>
        </tr>
	)
}

export default User;