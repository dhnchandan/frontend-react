import React from "react";

const UserDetails = ({ user }) => {
	return (
		<div className="row justify-content-center">
			<div className="col-lg-12 col-md-12 col-12">
				<div className="card border-0 shadow">
					<div className="card-header">
						<h3 className="card-title text-center font-weight-light">
							User Details
						</h3>
					</div>
					<div className="card-body">
						<div className="row justify-content-center align-items-center">
							<div className="col-lg-3 col-md-3 col-12">
								<img
									src={`http://demo.gridstacks.com/aws-demo-backend/${user.image}`}
									alt="Profile"
									height="250"
									width="100%"
								/>
							</div>
							<div className="col-lg-5 col-md-5 col-12">
								<div className="table-responsive">
									<table className="table table-borderless table-hover">
										<tbody>
											<tr>
												<td>Name:</td>
												<td>{user.name}</td>
											</tr>
											<tr>
												<td>Email:</td>
												<td>{user.email}</td>
											</tr>
											<tr>
												<td>Phone:</td>
												<td>{user.phone}</td>
											</tr>
											<tr>
												<td>Address:</td>
												<td>
													{user.address === ""
														? "N/A"
														: user.address}
												</td>
											</tr>
											<tr>
												<td>Street:</td>
												<td>
													{user.street === ""
														? "N/A"
														: user.street}
												</td>
											</tr>
											<tr>
												<td>City:</td>
												<td>
													{user.city === ""
														? "N/A"
														: user.city}
												</td>
											</tr>
											<tr>
												<td>Country:</td>
												<td>
													{user.country === ""
														? "N/A"
														: user.country}
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default UserDetails;
