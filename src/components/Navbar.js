import React from 'react'
import {Link} from 'react-router-dom'

const Navbar = () => {
    return (
        <nav className="navbar navbar-dark bg-dark navbar-expand-md">
            <div className="container-fluid">
                <Link to="/" className="navbar-brand">React</Link>
                <button type="button" className="navbar-toggler" data-toggle="collapse" data-target="#mainMenu">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div id="mainMenu" className="collapse navbar-collapse">
                    <ul className="nav navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to="/" className="nav-link">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/list" className="nav-link">List</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/add" className="nav-link">Add</Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Navbar;