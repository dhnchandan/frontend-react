import React, { Component } from "react";
import axios from "axios";

import User from "./../components/User";

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			users: [],
		};
	}

	componentDidMount() {
		const url = "http://demo.gridstacks.com/aws-demo-backend/fetch.php";
		axios
			.get(url)
			.then((res) => {
				// console.log(res.data)
				this.setState({
					users: res.data,
				});
			})
			.catch((err) => console.log(err));
	}

	render() {
		return (
			<div className="row justify-content-center">
				<div className="col-lg-12 col-md-12 col-12">
					<div className="card border-0 shadow">
						<div className="card-header">
							<h3 className="card-title text-center font-weight-light">
								User List
							</h3>
						</div>
						<div className="card-body">
							<div className="table-responsive">
								<table className="table table-borderless table-hover text-center">
									<thead className="bg-light">
										<tr>
											<td>Name</td>
											<td>Email</td>
											<td>Phone</td>
											<td>Address</td>
											<td>City</td>
											<td>Street</td>
											<td>Country</td>
											<td>Action</td>
										</tr>
									</thead>
									<tbody>
										{this.state.users &&
											this.state.users.map((user) => {
												return (
													<User
														user={user}
														key={user.id}
													/>
												);
											})}
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default List;
