import React, { Component } from "react";
import UserDetails from "./../components/UserDetails";

import axios from "axios";

class View extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: {},
		};
	}

	componentDidMount() {
		const url = `http://demo.gridstacks.com/aws-demo-backend/fetchSingle.php?id=${this.props.match.params.id}`;
		axios
			.get(url)
			.then((res) => {
				this.setState({
					user: res.data,
				});
			})
			.catch((err) => console.log(err));
	}

	render() {
		return (
			<div className="row justify-content-center">
				<div className="col-lg-12 col-md-12 col-12">
					<UserDetails user={this.state.user} />
				</div>
			</div>
		);
	}
}

export default View;
