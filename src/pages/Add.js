import React, { Component } from "react";
import axios from "axios";

import Demo from "./../static/img/demo.png";

class Add extends Component {
	constructor() {
		super();
		this.state = {
			name: "",
			email: "",
			phone: "",
			address: "",
			street: "",
			city: "",
			country: "",
			image: null,
		};
	}

	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value,
		});
	};

	handleFile = (e) => {
		this.setState({
			image: e.target.files[0],
		});
	};

	handleSubmit = (e) => {
		e.preventDefault();

		let fd = new FormData();
		fd.append("name", this.state.name);
		fd.append("email", this.state.email);
		fd.append("phone", this.state.phone);
		fd.append("address", this.state.address);
		fd.append("street", this.state.street);
		fd.append("city", this.state.city);
		fd.append("country", this.state.country);
		fd.append("image", this.state.image, this.state.image.name);

		const url = "http://demo.gridstacks.com/aws-demo-backend/create.php";

		axios
			.post(url, fd)
			.then((res) => {
				console.log(res.data);
				this.setState({
					success: res.data,
				});
			})
			.catch((err) => console.log(err));

		this.setState({
			name: "",
			email: "",
			phone: "",
			address: "",
			street: "",
			city: "",
			country: "",
			image: null,
		});

		document.getElementById("image").value = null;
	};

	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="row justify-content-center no-gutter">
					<div className="col-lg-3 col-md-3 col-12 mb-lg-0 mb-md-0 mb-3">
						<div className="card border-0 shadow">
							<div className="card-header">
								<h3 className="card-title text-center font-weight-light">
									User Picture
								</h3>
							</div>
							<div className="card-body">
								<div className="text-center">
									<img
										src={Demo}
										alt="Demo Profile"
										className="img-fluid"
									/>
								</div>
								<div className="form-group mt-2 mb-0">
									<input
										type="file"
										name="image"
										id="image"
										className="form-control-file form-control-sm"
										onChange={this.handleFile}
										required
									/>
								</div>
							</div>
						</div>
					</div>
					<div className="col-lg-6 col-md-6 col-12">
						<div className="card border-0 shadow">
							<div className="card-header">
								<h3 className="card-title text-center font-weight-light">
									User Form
								</h3>
							</div>
							<div className="card-body">
								<div className="form-group">
									<label>Name:</label>
									<input
										type="text"
										name="name"
										id="name"
										className="form-control"
										required
										onChange={this.handleChange}
										value={this.state.name}
									/>
								</div>
								<div className="form-group">
									<label>Email:</label>
									<input
										type="email"
										name="email"
										id="email"
										className="form-control"
										required
										onChange={this.handleChange}
										value={this.state.email}
									/>
								</div>
								<div className="form-group">
									<label>Phone</label>
									<input
										type="text"
										name="phone"
										id="phone"
										className="form-control"
										required
										onChange={this.handleChange}
										value={this.state.phone}
									/>
								</div>
								<div className="form-group">
									<label>Address</label>
									<input
										type="text"
										name="address"
										id="address"
										className="form-control"
										onChange={this.handleChange}
										value={this.state.address}
									/>
								</div>
								<div className="form-group">
									<label>Street</label>
									<input
										type="text"
										name="street"
										id="street"
										className="form-control"
										onChange={this.handleChange}
										value={this.state.street}
									/>
								</div>
								<div className="form-group">
									<label>City</label>
									<input
										type="text"
										name="city"
										id="city"
										className="form-control"
										onChange={this.handleChange}
										value={this.state.city}
									/>
								</div>
								<div className="form-group">
									<label>Country</label>
									<input
										type="text"
										name="country"
										id="country"
										className="form-control"
										onChange={this.handleChange}
										value={this.state.country}
									/>
								</div>
								<div className="form-group">
									<button
										type="submit"
										className="btn btn-success btn-block"
									>
										Register
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		);
	}
}

export default Add;
