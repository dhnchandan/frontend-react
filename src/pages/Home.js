import React from "react";

const Home = () => {
	return (
		<div className="jumbotron">
			<h1 className="text-muted display-4 mb-3">
				AWS Project - Cloud Computing
			</h1>
			<p className="lead text-justify">
				Welcome. This is a cloud computing project for AWS learning.
				Here EC2 Instance is used for web server. PHP as backend and
				React JS as frontend.
			</p>
			<p className="lead text-justify">
				The static frontend is server from AWS S3. For database AWS RDS
				is used.
			</p>
			<p className="lead text-justify">
				The database is secured as accessible only from our web server.
			</p>
		</div>
	);
};

export default Home;
